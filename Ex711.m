clear
clf
bb=imread('bridge.jpg');
figure(1),image(bb),colormap(gray)
axis off
aa=im2double(bb);
FF=fft2(aa(:,:,1),512,512);
FF=fftshift(FF);
ct=[256,256];
mask=zeros(512,512);
n=6;
d0=50;
for i=1:512
    for j=1:512
        mask(i,j)=1/(1+[sqrt((i-ct(1))^2+(j-ct(2))^2)/d0]^(2*n));
    end
end
figure(2),surf(mask)
OO=FF.*mask;
oo=ifft2(OO,512,512);
output=abs(oo);
figure(3),imagesc(output),colormap(gray)
axis off